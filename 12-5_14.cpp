#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
extern void __declspec(dllexport) input5(int *, int, int , bool);
extern bool __declspec(dllexport) check5(int *, int, int);
//ф-я , яка заповнює массив випадковими значеннями (або просто виводить )
int main() {
  srand(time(NULL));
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
  int n,m,k;
  int a[5][5],b[4][4];
  int c[3][3] = {1,2,3,2,1,2,3,2,1};
  
  input5(a[0],5,1,true);
  input5(b[0],4,2,true);
  input5(c[0],3,3,false);
  check5(a[0],5,1);
  check5(b[0],4,2);
  check5(c[0],3,3);
  cout<<endl;
  system("pause");
  return 0;
}
