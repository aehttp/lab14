/*Створити та вивести одномірний масив з сум додатних елементів кожної
паралельної діагоналі, які вище головної діагоналі матриці v35(n,m).*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
extern void __declspec(dllexport) input2(int *, int), diag2(int *, int);
int main(){
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,j,k,n;
	printf("введіть розмір масиву : ");
	cin>>n;
	int a[n][n];
	input2(a[0],n);
	diag2(a[0],n);
	system("pause");
	return 0;
}
