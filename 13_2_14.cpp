//варіант 4  (-10 = 19) 
/*В заданий багатовимірній матриці g19(3,6,3) знайти середнє квадратичне
значення усіх елементів. Вивести вихідну матрицю та знайдене середнє
квадратичне значення.*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
extern void __declspec(dllexport) ser2(int [][6][3], int,int,int);
void gotoxy(int x, int y){
	COORD scrn;
	HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);
	scrn.X=x;
	scrn.Y=y;
	SetConsoleCursorPosition(hOuput,scrn);
}
int main(){
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int g[3][6][3];
	int i,j,k;
	printf("Сформований масив: \n");
	for(i=0;i<3;i++){
		for(j=0;j<6;j++){
			for(k=0;k<3;k++){
				g[i][j][k]= rand()%100-50;
				gotoxy(((j+k)*4+1),(((1+i)*6)-j));
				printf("%d",g[i][j][k]);
			}
			gotoxy(0,18);
			cout<<endl<<endl;
		}
	}
	ser2(g,3,6,3);
	system("pause");
	return 0;
}
