//к-сть * ! ? в рядку
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
using namespace std;
extern int __declspec(dllexport) s_k4(string, char);

int main(){
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	string str;
	printf("Введіть рядок: ");
	getline(cin,str);
	printf("Кількість знаків * = %4d \n",s_k4(str,'*'));
	printf("Кількість знаків ! = %4d \n",s_k4(str,'!'));
	printf("Кількість знаків ? = %4d \n",s_k4(str,'?'));
	system("pause");
	return 0;
}
