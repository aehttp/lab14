//Створити та вивести одномірний масив з сум елементів кожної парної 
//діагоналі, що паралельні головній діагоналі матриці v41(n,m).

#include <iostream>
#include <string>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
extern int __declspec(dllexport) p_m3(int *, int,int),i_m3(int *, int,int),st_v3(int *, int,int);
int main()
{
  system("cls");
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
  int n,m; 
  printf("Введіть n: ");
  cin>>n;
  printf("Введіть m: ");
  cin>>m;
  int a[n][m];
  
  printf("Введіть матрицю a(%d,%d)\n",n,m);
  i_m3(&a[0][0],n,m);
  system("cls");
  printf("Задана матриця: a(%d,%d)\n",n,m);
  p_m3(&a[0][0],n,m);
  st_v3(&a[0][0],n,m);

   system("pause");
  
return 0;
}
